# Phoenix-RS

This is a re-write of Phoenix bot in Rust. Why? Because I hate myself.

### Phoenix-RS is still in very early development (yes, I've only written the boiler-plate code)

Phoenix-RS aims to be a simple and elegant Hypixel Guild Bot runner. Features will include:

- Automatic Role Sync in Discord Server
- Easy way to manage Guild XP and see which members are under-performing
- View guild chat logs
- Run commands and send chat messages from Discord
- Have all the tools needed to moderate your Guild
- Come with some special Features
    - Guild's personal stats bot (using the `.stats` command)
    - Notifying moderators when there is an incident
    - Welcoming new players into the guild
    - Possibly even playing some chat-based games!
- And much more!

There is no ETA for this project, but check back frequently, because its about to pick up (hopefully).