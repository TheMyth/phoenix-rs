use azalea_client::Account;
use chrono::Local;
use diesel::{
    prelude::*,
    r2d2::{ConnectionManager, Pool},
    QueryDsl, RunQueryDsl,
};
use dotenvy::dotenv;
use futures::future::join_all;
use log::LevelFilter;
use simplelog::*;

use std::fs::OpenOptions;
use std::sync::{Arc, Mutex};
use std::{env, error::Error};

#[macro_use]
extern crate log;

mod discord;
mod minecraft;
mod models;
mod schema;
use models::*;

#[tokio::main]
async fn main() {
    dotenv().ok();
    init_logger();
    let manager = ConnectionManager::new(
        env::var("DATABASE_URL").expect("Environment variable \"DATABASE_URL\" must be set!"),
    );
    let dbpool = Pool::builder()
        .test_on_check_out(true)
        .build(manager)
        .expect("Could not build connection pool");
    let discord_token =
        env::var("DISCORD_TOKEN").expect("Environment variable \"DISCORD_TOKEN\" must be set!");
    let accounts: Vec<Account> = get_accounts(&mut (dbpool.clone()))
        .await
        .expect("Could not grab accounts from database!");
    if accounts.is_empty() {
        error!("No Minecraft Bots in database? Is this an issue?? Continuing...");
    }
    let mc_app = Arc::new(Mutex::new(minecraft::MinecraftApp::default()));

    loop {
        let mc_app_clone = mc_app.clone();
        // Run services simultaneously
        let (minecraft, discord) = tokio::join!(
            minecraft::create_swarm(accounts.clone(), &mc_app_clone, dbpool.clone()),
            discord::create_client(&discord_token, dbpool.clone())
        );

        if minecraft.is_ok() && discord.is_ok() {
            info!("Serivices exited gracefully, re-running");
            continue;
        }

        if let Some(error) = minecraft.err() { error!("Azalea exited with error: {}", error);}
        if let Some(error) = discord.err() { error!("Serenity exited with error: {}", error);}
    }
}

async fn get_accounts(dbpool: &mut PgPool) -> Result<Vec<Account>, Box<dyn Error>> {
    let mut connection = dbpool.get()?;
    let bot_entries: Vec<McBot>;
    {
        // This is done to prevent so much junk getting fed into autcomplete (I think)
        use crate::schema::mc_bots::dsl::*;
        bot_entries = mc_bots.select(McBot::as_select()).load::<McBot>(&mut connection)?;
    }
    // Lol
    if ! bot_entries.is_empty() {
        // Yippee
        let mut handles = Vec::new();

        // Half-hazard approach to multithreading
        for bot in bot_entries {
            let username = bot.username.clone();

            let handle = tokio::spawn(async move {
               Account::microsoft(&username).await.ok()
            });
            handles.push(handle);
        }

        // This looks like a mess but trust me
        let results = join_all(handles)
            .await
            .into_iter()
            // Result<Option<Account>, JoinHandle> => Option<Account>
            .filter_map(|elem| {
                elem.ok()
            })
            // Option<Account> => Account || nothing
            .flatten()
            .collect();

        Ok(results)
    } else {
        Ok(Vec::new())
    }
}


fn init_logger() {
    CombinedLogger::init(vec![
        TermLogger::new(
            LevelFilter::Error,
            Config::default(),
            TerminalMode::Mixed,
            ColorChoice::Auto,
        ),
        TermLogger::new(
            LevelFilter::Info,
            Config::default(),
            TerminalMode::Mixed,
            ColorChoice::Auto,
        ),
        TermLogger::new(
            LevelFilter::Warn,
            Config::default(),
            TerminalMode::Mixed,
            ColorChoice::Auto,
        ),
        WriteLogger::new(
            LevelFilter::Error,
            Config::default(),
            OpenOptions::new()
                .append(true)
                .create(true)
                .open(format!("log-{}.txt", Local::now().format("%Y-%m-%d")))
                .unwrap(),
        ),
        TermLogger::new(
            LevelFilter::Trace,
            Config::default(),
            TerminalMode::Mixed,
            ColorChoice::Auto,
        ),
    ])
    .unwrap();
}
