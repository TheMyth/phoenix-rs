use diesel::{
    prelude::*,
    r2d2::ConnectionManager,
};
use r2d2::Pool;

pub type PgPool = Pool<ConnectionManager<PgConnection>>;

#[derive(Queryable, Selectable)]
#[diesel(table_name = crate::schema::guilds)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct Guild {
    pub id: i32,
    pub pardon_new_members: bool,
    pub hypixel_guild_id: String,
    pub log_channel: i64,
    pub auto_run_bot: bool,
    pub staff_role_id: i64,
    pub staff_ping: bool,
    pub other_options: Option<serde_json::Value>
}

#[derive(Queryable, Selectable)]
#[diesel(table_name = crate::schema::discords)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct Discord {
    pub id: i32,
    pub discord_id: i64,
    // Id in database
    pub guild_id: i32,
    pub vip_role: Option<i64>,
    pub vipplus_role: Option<i64>,
    pub mvp_role: Option<i64>,
    pub mvpplus_role: Option<i64>,
    pub mvppp_role: Option<i64>,
    // No way this works first time
    pub other_roles: Option<serde_json::Value>
}

#[derive(Queryable, Selectable)]
#[diesel(table_name = crate::schema::mc_bots)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct McBot {
    pub id: i32,
    pub uuid: String,
    pub guild_id: i32,
    pub password: String,
    pub username: String,
    pub auto_run: bool,
    pub online: bool,
    pub api_key: Option<String>,
    pub other_options: Option<serde_json::Value>
}

#[derive(Queryable, Selectable)]
#[diesel(table_name = crate::schema::players)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct Player {
    pub id: i32,
    pub uuid: String,
    pub whitelist: bool,
    pub discord_id: i32,
    pub other_options: Option<serde_json::Value>
}

#[derive(Queryable, Selectable)]
#[diesel(table_name = crate::schema::roles)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct Role {
    pub id: i32,
    pub is_staff: bool,
    pub discord_role_id: String,
    pub role_prefix: String,
    pub role_name: String,
    pub guild_id: i32,
    pub other_options: Option<serde_json::Value>
}

#[derive(Queryable, Selectable)]
#[diesel(table_name = crate::schema::guild_xp_data)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct GuildXPData {
    pub id: i32,
    pub guild_id: i32,
    pub minxp: i64,
    pub role_id: i32,
    // pub other_options: Option<serde_json::Value>
}
