use azalea::ChatPacket;
use std::fmt::Display;
use std::sync::{Arc, Mutex};

use super::MinecraftApp;

pub struct HandleChatError {}

impl Display for HandleChatError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "There was an error of handling chat messages: ")
    }
}

pub fn handle_guild_message(
    bot: azalea::Client,
    m: ChatPacket,
    app: Arc<Mutex<MinecraftApp>>,
) -> Result<(), HandleChatError> {
    // Find bot id in mc_bots table
    // Find guild id
    // If log channel and log messages = true
    // Send message into queue
    // app.lock().unwrap().bots.get_mut(/* uuid */).unwrap().mc_to_discord_queue.push(m);

    Ok(())
}
