use azalea::{
    prelude::*,
    protocol::{resolver, ServerAddress},
    Account, Client, Event, JoinError,
};
use azalea_client::*;
use diesel::{ExpressionMethods, QueryDsl, SelectableHelper};

use crate::{models::McBot, schema::mc_bots, PgPool};
use crossbeam::queue::SegQueue;
use std::{
    collections::HashMap,
    error::Error,
    sync::{Arc, Mutex},
};
use tokio::sync::mpsc;
use uuid::Uuid;

mod bot_impl;
mod commands;
type Username = String;
#[derive(Default)]
pub struct MinecraftApp {
    // Bot UUID, actual Bot
    bots: HashMap<Uuid, BotStruct>,
}
pub enum BotState {
    Connecting,
    Joined,
    Left,
    Error(anyhow::Error),
}

pub struct BotStruct {
    username: Username,
    state: BotState,
    discord_to_mc_queue: SegQueue<String>,
    mc_to_discord_queue: SegQueue<ChatPacket>,
}

impl BotStruct {
    fn new(username: Username) -> Self {
        Self {
            username,
            state: BotState::Connecting,
            discord_to_mc_queue: SegQueue::new(),
            mc_to_discord_queue: SegQueue::new(),
        }
    }
    fn set_state(&mut self, state: BotState) {
        self.state = state;
    }
}
pub enum Message {
    Joined(Uuid),
    Left(Uuid),
    // Message(Uuid, ChatPacket),
    Error(Uuid, JoinError),
    Tick,
}

#[derive(Clone, Component)]
pub struct State {
    tx: mpsc::Sender<Message>,
    pool: PgPool,
}
impl State {
    fn new(tx: mpsc::Sender<Message>, pool: PgPool) -> Self {
        Self { tx, pool }
    }
}

pub async fn create_swarm(
    accounts: Vec<Account>,
    app: &Arc<Mutex<MinecraftApp>>,
    db_pool: PgPool,
) -> Result<(), Box<dyn Error>> {
    let (tx, mut rx) = mpsc::channel::<Message>(16);
    for account in accounts {
        let tx = tx.clone();
        let pool = db_pool.clone();
        let newapp = app.clone();
        newapp.lock().unwrap().bots.insert(
            account.uuid.unwrap(),
            BotStruct::new((&account.username).into()),
        );
        tokio::spawn(async move {
            let account = &account.clone();
            let state = State::new(tx.clone(), pool.clone());
            let tx = state.tx.clone();
            // Here comes the fun part
            let bot = create_client(state, account, "mc.hypixel.net", newapp).await;

            match bot {
                Ok(()) => tx.send(Message::Left(account.uuid.unwrap())).await,
                Err(e) => tx.send(Message::Error(account.uuid.unwrap(), e)).await,
            }
        });
    }
    while let Some(event) = rx.recv().await {
        match event {
            Message::Joined(uuid) => app
                .lock()
                .unwrap()
                .bots
                .get_mut(&uuid)
                .unwrap()
                .set_state(BotState::Joined),
            Message::Left(uuid) => app
                .lock()
                .unwrap()
                .bots
                .get_mut(&uuid)
                .unwrap()
                .set_state(BotState::Left),
            Message::Error(uuid, e) => app
                .lock()
                .unwrap()
                .bots
                .get_mut(&uuid)
                .unwrap()
                .set_state(BotState::Error(e.into())),
            Message::Tick => {
                // Check for updates in DB
                // ^^ DON'T, GO THROUGH DISCORD BOT
            }
        }
    }
    Ok(())
}

pub async fn add_to_swarm(account: Account, app: &Arc<Mutex<MinecraftApp>>, db_pool: PgPool) {
    let (tx, mut rx) = mpsc::channel::<Message>(16);
    let tx = tx.clone();
    let pool = db_pool.clone();
    let newapp = app.clone();
    newapp.lock().unwrap().bots.insert(
        account.uuid.unwrap(),
        BotStruct::new((&account.username).into()),
    );
    tokio::spawn(async move {
        let account = &account.clone();
        let state = State::new(tx.clone(), pool.clone());
        let tx = state.tx.clone();
        // Here comes the fun part
        let bot = create_client(state, account, "mc.hypixel.net", newapp).await;

        match bot {
            Ok(()) => tx.send(Message::Left(account.uuid.unwrap())).await,
            Err(e) => tx.send(Message::Error(account.uuid.unwrap(), e)).await,
        }
    });
    while let Some(event) = rx.recv().await {
        match event {
            Message::Joined(uuid) => app
                .lock()
                .unwrap()
                .bots
                .get_mut(&uuid)
                .unwrap()
                .set_state(BotState::Joined),
            Message::Left(uuid) => app
                .lock()
                .unwrap()
                .bots
                .get_mut(&uuid)
                .unwrap()
                .set_state(BotState::Left),
            Message::Error(uuid, e) => app
                .lock()
                .unwrap()
                .bots
                .get_mut(&uuid)
                .unwrap()
                .set_state(BotState::Error(e.into())),
            Message::Tick => {
                // Check for updates in DB
                // ^^ DON'T, GO THROUGH DISCORD BOT
            }
        }
    }
}

async fn create_client(
    state: State,
    account: &Account,
    address: impl TryInto<ServerAddress>,
    app: Arc<Mutex<MinecraftApp>>,
) -> Result<(), JoinError> {
    let address: ServerAddress = address.try_into().map_err(|_| JoinError::InvalidAddress)?;
    let resolved_address = resolver::resolve_address(&address).await?;

    let (tx, rx) = mpsc::channel(1);
    let ecs_lock = start_ecs(init_ecs_app(), rx, tx.clone());
    let (bot, mut rx) =
        Client::start_client(ecs_lock, account, &address, &resolved_address, tx).await?;

    while let Some(event) = rx.recv().await {
        // Dont handle event here, do it in the handle function (much easier and cleaner)
        tokio::spawn(handle(
            bot.clone(),
            event.clone(),
            state.clone(),
            app.clone(),
        ));
    }

    Ok(())
}

async fn handle(
    bot: Client,
    event: Event,
    state: State,
    mc_app: Arc<Mutex<MinecraftApp>>,
) -> anyhow::Result<()> {
    match event {
        Event::Login => {
            let _ = state.tx.send(Message::Joined(bot.profile.uuid)).await;
            {
                // Find account in database
                // If API_KEY is set, then leave alone
                // else grab API key
            }
            Ok(())
        }
        Event::Chat(message) => {
            if !message.message().to_string().starts_with("Guild >") {
                return Ok(());
            }
            bot_impl::handle_guild_message(bot.clone(), message, mc_app)
                .unwrap_or_else(|_| bot.send_command_packet("gc Could not parse guild message!"));
            Ok(())
        }
        Event::RemovePlayer(m) => {
            if m.uuid == bot.profile.uuid {
                let _ = state.tx.send(Message::Left(bot.profile.uuid)).await;
            }
            Ok(())
        }
        Event::Init => Ok(()),
        Event::Tick => {
            let _ = state.tx.send(Message::Tick).await;
            Ok(())
        }
        Event::Packet(_) => Ok(()),
        Event::AddPlayer(_) => Ok(()),
        Event::UpdatePlayer(_) => Ok(()),
        Event::Death(_) => Ok(()),
        Event::KeepAlive(_) => Ok(()),
    }
}
