use azalea_client::ChatPacket;

use crate::PgPool;

pub fn handle_chat(bot: azalea::Client, m: ChatPacket, db: PgPool) -> anyhow::Result<()> {
    // Not Implemented
    bot.send_command_packet(format!("gc {}", m.message().to_string()).as_str());
    Ok(())
}
