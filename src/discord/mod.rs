mod commands;

use serenity::async_trait;
use serenity::model::application::command::Command;
use serenity::model::application::interaction::{Interaction, InteractionResponseType};
use serenity::model::gateway::Ready;
use serenity::prelude::*;
use std::error::Error;

use crate::models::PgPool;

// temporary
struct Handler {
    db_pool: PgPool,
}

#[async_trait]
impl EventHandler for Handler {
    async fn interaction_create(&self, ctx: Context, interaction: Interaction) {
        if let Interaction::ApplicationCommand(command) = interaction {
            info!("Recieved command interaction: {:#?}", command);

            let content = match command.data.name.as_str() {
                "ping" => commands::ping::run(&command.data.options, ctx.clone()),
                _ => "not implemented :(".to_string(),
            };

            if let Err(why) = command
                .create_interaction_response(&ctx.http, |response| {
                    response
                        .kind(InteractionResponseType::ChannelMessageWithSource)
                        .interaction_response_data(|message| message.content(content))
                })
                .await
            {
                error!("Cannot respond to slash command: {}", why)
            }
        }
    }
    async fn ready(&self, ctx: Context, ready: Ready) {
        println!("{} is connected!", ready.user.name);

        let global_commands = Command::create_global_application_command(&ctx.http, |command| {
            commands::ping::register(command)
        })
        .await;
        println!("Registered global commands: {:?}", global_commands)
    }
}

pub async fn create_client(token: &String, db_pool: PgPool) -> Result<(), Box<dyn Error>> {
    let intents = GatewayIntents::all();
    let mut client = Client::builder(token, intents)
        .event_handler(Handler { db_pool })
        .await
        .expect("Error Creating Client");

    match client.start().await {
        Ok(()) => Ok(()),
        Err(why) => Err(Box::new(why)),
    }
}
