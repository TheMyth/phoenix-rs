// @generated automatically by Diesel CLI.

diesel::table! {
    discords (id) {
        id -> Int4,
        discord_id -> Int8,
        guild_id -> Int4,
        vip_role -> Nullable<Int8>,
        vipplus_role -> Nullable<Int8>,
        mvp_role -> Nullable<Int8>,
        mvpplus_role -> Nullable<Int8>,
        mvppp_role -> Nullable<Int8>,
        other_roles -> Nullable<Jsonb>,
    }
}

diesel::table! {
    guild_xp_data (id) {
        id -> Int4,
        guild_id -> Int4,
        minxp -> Int8,
        role_id -> Int4,
    }
}

diesel::table! {
    guilds (id) {
        id -> Int4,
        pardon_new_members -> Bool,
        hypixel_guild_id -> Varchar,
        log_channel -> Int8,
        auto_run_bot -> Bool,
        staff_role_id -> Int8,
        staff_ping -> Bool,
        other_options -> Nullable<Jsonb>,
    }
}

diesel::table! {
    mc_bots (id) {
        id -> Int4,
        uuid -> Varchar,
        guild_id -> Int4,
        password -> Varchar,
        username -> Varchar,
        auto_run -> Bool,
        online -> Bool,
        api_key -> Nullable<Varchar>,
        other_options -> Nullable<Jsonb>,
    }
}

diesel::table! {
    players (id) {
        id -> Int4,
        uuid -> Varchar,
        whitelist -> Bool,
        guild_id -> Int4,
        discord_id -> Int4,
        other_options -> Nullable<Jsonb>,
    }
}

diesel::table! {
    roles (id) {
        id -> Int4,
        is_staff -> Bool,
        discord_role_id -> Varchar,
        role_prefix -> Varchar,
        role_name -> Text,
        guild_id -> Int4,
        other_options -> Nullable<Jsonb>,
    }
}

diesel::joinable!(discords -> guilds (guild_id));
diesel::joinable!(guild_xp_data -> guilds (guild_id));
diesel::joinable!(guild_xp_data -> roles (role_id));
diesel::joinable!(mc_bots -> guilds (guild_id));
diesel::joinable!(players -> discords (discord_id));
diesel::joinable!(players -> guilds (guild_id));
diesel::joinable!(roles -> guilds (guild_id));

diesel::allow_tables_to_appear_in_same_query!(
    discords,
    guild_xp_data,
    guilds,
    mc_bots,
    players,
    roles,
);
