-- Your SQL goes here
CREATE TABLE discords(
    id SERIAL PRIMARY KEY,
    discord_id BIGINT NOT NULL,
    guild_id SERIAL REFERENCES guilds(id),
    vip_role BIGINT,
    vipplus_role BIGINT,
    mvp_role BIGINT,
    mvpplus_role BIGINT,
    mvppp_role BIGINT,
    other_roles JSONB
);
