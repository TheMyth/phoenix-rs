-- Your SQL goes here
CREATE TABLE roles(
    id SERIAL PRIMARY KEY,
    is_staff BOOLEAN NOT NULL,
    discord_role_id VARCHAR(255) NOT NULL,
    role_prefix VARCHAR(255) NOT NULL,
    role_name TEXT NOT NULL,
    guild_id SERIAL REFERENCES guilds(id),
    other_options JSONB
);
