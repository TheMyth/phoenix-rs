-- Your SQL goes here
CREATE TABLE players(
    id SERIAL PRIMARY KEY,
    uuid VARCHAR(255) NOT NULL,
    whitelist BOOLEAN NOT NULL DEFAULT FALSE,
    guild_id SERIAL REFERENCES guilds(id),
    discord_id SERIAL REFERENCES discords(id),
    other_options JSONB
);
