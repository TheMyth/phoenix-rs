-- Your SQL goes here
CREATE TABLE mc_bots(
    id SERIAL PRIMARY KEY,
    uuid VARCHAR(255) NOT NULL,
    guild_id SERIAL REFERENCES guilds(id),
    password VARCHAR(255) NOT NULL,
    username VARCHAR(255) NOT NULL,
    auto_run BOOLEAN NOT NULL,
    online BOOLEAN NOT NULL,
    api_key VARCHAR(255),
    other_options JSONB
);
