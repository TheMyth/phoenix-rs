-- Your SQL goes here
CREATE TABLE guild_xp_data(
    id SERIAL PRIMARY KEY,
    guild_id SERIAL REFERENCES guilds(id),
    minxp BIGINT NOT NULL,
    role_id SERIAL REFERENCES roles(id)
);
