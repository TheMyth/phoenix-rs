-- Your SQL goes herCREATE TABLE guilds(
CREATE TABLE guilds(
    id SERIAL PRIMARY KEY,
    pardon_new_members BOOLEAN NOT NULL,
    hypixel_guild_id VARCHAR(255) NOT NULL,
    log_channel BIGINT NOT NULL,
    auto_run_bot BOOLEAN NOT NULL,
    staff_role_id BIGINT NOT NULL,
    staff_ping BOOLEAN NOT NULL,
    other_options JSONB
);
